/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.cadsr;

import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterElement;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.lib.AdapterValueDomain;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.lib.Constants.Language;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * A single element from the caDSR.
 */
public class CadsrElement extends CadsrElementDescription implements AdapterElement {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Its datatype (VALUEDOMAIN/Datatype)
   */
  private String datatype;

  /**
   * Its format
   */
  private String format;
  /**
   * The number of maximum characters
   */
  private String maxCharacters;
  /**
   * The list of permissible value. Empty for described value domains.
   */
  private List<PermissibleValueItem> permissibleValues = new ArrayList<>();
  /**
   * The type of the value domain of this element.
   */
  private ValueDomainType type;
  /**
   * The public ID of the value domain.
   */
  private String valueDomainPublicId;
  /**
   * The version of this element.
   */
  private String version;

  /* (non-Javadoc)
   * @see de.samply.mdr.lib.AdapterElement#createValueDomain()
   */
  @Override
  public AdapterValueDomain createValueDomain() {
    AdapterValueDomain target = new AdapterValueDomain();

    if (type == ValueDomainType.DESCRIBED) {
      if ("NUMBER".equals(datatype)) {
        target.setDatatype(DatatypeField.INTEGER);
        target.setWithinRange(true);
        target.setRangeFrom(0);
        target.setRangeTo(BigInteger.TEN.pow(Integer.parseInt(maxCharacters)));
      }

    } else {
      target.setDatatype(DatatypeField.LIST);
      target.setPermittedValues(new ArrayList<AdapterPermissibleValue>());

      for (PermissibleValueItem item : permissibleValues) {
        ArrayList<AdapterDefinition> definitions = new ArrayList<>();
        AdapterDefinition def = new AdapterDefinition();
        def.setLanguage(Language.EN);
        def.setDesignation(item.getDesignation());
        def.setDefinition(item.getDefinition());
        definitions.add(def);

        AdapterPermissibleValue pv = new AdapterPermissibleValue();
        pv.setValue(item.getValue());
        pv.setDefinitions(definitions);
        target.getPermittedValues().add(pv);
      }
    }

    return target;
  }

  /* (non-Javadoc)
   * @see de.samply.mdr.lib.AdapterElement#getDefinitions()
   */
  @Override
  public List<AdapterDefinition> getDefinitions() {
    List<AdapterDefinition> target = new ArrayList<>();
    AdapterDefinition def = new AdapterDefinition();

    def.setLanguage(Language.EN);
    def.setDefinition(getDefinition());
    def.setDesignation(getDesignation());

    target.add(def);

    return target;
  }

  /**
   * @return the datatype
   */
  public String getDatatype() {
    return datatype;
  }

  /**
   * @param datatype the datatype to set
   */
  public void setDatatype(String datatype) {
    this.datatype = datatype;
  }

  /**
   * @return the format
   */
  public String getFormat() {
    return format;
  }

  /**
   * @param format the format to set
   */
  public void setFormat(String format) {
    this.format = format;
  }

  /**
   * @return the permissibleValues
   */
  public List<PermissibleValueItem> getPermissibleValues() {
    return permissibleValues;
  }

  /**
   * @param permissibleValues the permissibleValues to set
   */
  public void setPermissibleValues(List<PermissibleValueItem> permissibleValues) {
    this.permissibleValues = permissibleValues;
  }

  /**
   * @return the maxCharacters
   */
  public String getMaxCharacters() {
    return maxCharacters;
  }

  /**
   * @param maxCharacters the maxCharacters to set
   */
  public void setMaxCharacters(String maxCharacters) {
    this.maxCharacters = maxCharacters;
  }

  /**
   * @return the type
   */
  public ValueDomainType getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(ValueDomainType type) {
    this.type = type;
  }

  /**
   * @return the valueDomainPublicId
   */
  public String getValueDomainPublicId() {
    return valueDomainPublicId;
  }

  /**
   * @param valueDomainPublicId the valueDomainPublicId to set
   */
  public void setValueDomainPublicId(String valueDomainPublicId) {
    this.valueDomainPublicId = valueDomainPublicId;
  }

  /* (non-Javadoc)
   * @see de.samply.mdr.lib.AdapterElement#getIdentification()
   */
  @Override
  public String getIdentification() {
    return "cadsr://dataelement?publicId=" + getPublicId() + "&version=" + getVersion();
  }

  /**
   * @return the version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @param version the version to set
   */
  public void setVersion(String version) {
    this.version = version;
  }

  public enum ValueDomainType {ENUMERATED, DESCRIBED}

  public static class PermissibleValueItem implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String value;

    private String definition;

    private String designation;

    /**
     * @return the value
     */
    public String getValue() {
      return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
      this.value = value;
    }

    /**
     * @return the definition
     */
    public String getDefinition() {
      return definition;
    }

    /**
     * @param definition the definition to set
     */
    public void setDefinition(String definition) {
      this.definition = definition;
    }

    /**
     * @return the designation
     */
    public String getDesignation() {
      return designation;
    }

    /**
     * @param designation the designation to set
     */
    public void setDesignation(String designation) {
      this.designation = designation;
    }
  }

  public static class DescribedValueDomain {

    private String format;

    private String maxCharacters;

    /**
     * @return the format
     */
    public String getFormat() {
      return format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(String format) {
      this.format = format;
    }

    /**
     * @return the maxCharacters
     */
    public String getMaxCharacters() {
      return maxCharacters;
    }

    /**
     * @param maxCharacters the maxCharacters to set
     */
    public void setMaxCharacters(String maxCharacters) {
      this.maxCharacters = maxCharacters;
    }

  }

}
