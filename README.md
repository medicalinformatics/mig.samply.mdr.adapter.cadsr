# Samply caDSR Client

This client offers convenient methods to access the caDSR REST interface.

When using this client, always keep in mind, that the caDSR is *very* slow
(~20 seconds per request).


## Build

Use maven to build the jar:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>cadsr-client</artifactId>
    <version>$VERSION</version>
</dependency>
```
