# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2019-04-25
### Changed
- [Google java code style for intellij](https://github.com/google/styleguide/blob/gh-pages/intellij-java-google-style.xml) was applied
### Fixed
- caDSR base url changed to https scheme

## [1.1.0] - 2018-12-17
### Added
- slf4j logger